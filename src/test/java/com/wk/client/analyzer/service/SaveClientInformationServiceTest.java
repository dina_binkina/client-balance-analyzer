package com.wk.client.analyzer.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.wk.client.analyzer.config.AppConfig;
import com.wk.client.analyzer.config.WebInitializationConfig;
import com.wk.client.analyzer.pojo.ClientInfo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;
import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfig.class})
@WebAppConfiguration
public class SaveClientInformationServiceTest {

    @Value("${balanceLimit}")
    String balanceLimit;

    @Autowired
    SaveClientInformationService saveClientInformationService;

    @Autowired
    PublishClientInformationService publishClientInformationService;

    @Test
    public void saveReadCacheTest() {
        String responseBody = "<ClientsInfoResponse><clientsInfo><clientsInfo><clientId>a9BnInPYJ</clientId><login>firstLogin</login><tariff>10</tariff><balance>1000</balance></clientsInfo><clientsInfo><clientId>XQa4bCxri</clientId><login>secondLogin</login><tariff>20</tariff><balance>1000</balance></clientsInfo><clientsInfo><clientId>WKfnbk6jh</clientId><login>thirdLogin</login><tariff>30</tariff><balance>1000</balance></clientsInfo></clientsInfo></ClientsInfoResponse>";
        saveClientInformationService.saveToCache(responseBody);

        String message = publishClientInformationService.getMessage();

        try {
            HashMap<String, Object> result = new ObjectMapper().readValue(message, HashMap.class);

            assertEquals(result.size(), 3);

            result.forEach((key, value) -> {
                assertTrue((Long.valueOf(String.valueOf(((ArrayList)value).get(0))) < Long.valueOf(balanceLimit)));
            });

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}