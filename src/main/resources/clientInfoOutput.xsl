<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet
   xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

   <xsl:template match="/">
       <xsl:text>&#xa;</xsl:text>
       <xsl:for-each select="ClientsInfoResponse/clientsInfo/clientsInfo">
           <xsl:value-of select="clientId"/>/<xsl:value-of select="balance"/><xsl:text>&#xa;</xsl:text>
       </xsl:for-each>
   </xsl:template>
</xsl:stylesheet>