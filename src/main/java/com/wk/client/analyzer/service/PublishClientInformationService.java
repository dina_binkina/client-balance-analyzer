package com.wk.client.analyzer.service;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.json.JsonObject;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.util.Set;

@Slf4j
@Service
public class PublishClientInformationService {

    @Value("${amqpHost}")
    String amqpHost;

    @Value("${bucketDocumentId}")
    String bucketDocumentId;

    @Value("${balanceLimit}")
    String balanceLimit;

    @Autowired
    private BucketInitializer bucketInitializer;

    private final static String QUEUE_NAME = "client-balance-info";

    @Scheduled(fixedRate = 10000)
    public void publishClientInfo() {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(amqpHost);
        try {
            Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();

            channel.queueDeclare(QUEUE_NAME, false, false, false, null);
            String message = getMessage();
            if (message != null && message.length() > 0) {
                channel.basicPublish("", QUEUE_NAME, null, message.getBytes(StandardCharsets.UTF_8));
                log.debug(" [x] Sent '" + message + "'");
            }

            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getMessage() {
        Bucket bucket = bucketInitializer.getBucket();

        JsonDocument retrieved = bucket.get(bucketDocumentId);
        JsonObject content = retrieved.content();
        Set<String> clientIds = content.getNames();

        JSONObject clientBalanceInfo = new JSONObject();

        for (String clientId : clientIds) {
            Long balance = Long.valueOf((String) (content.get(clientId)));
            if (balance < Long.valueOf(balanceLimit)) {
                clientBalanceInfo.append(clientId, balance);
            }
        }

        return clientBalanceInfo.toString();
    }
}
