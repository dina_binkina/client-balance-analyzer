package com.wk.client.analyzer.service;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.CouchbaseCluster;
import com.couchbase.client.java.env.CouchbaseEnvironment;
import com.couchbase.client.java.env.DefaultCouchbaseEnvironment;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class BucketInitializer {

    @Value("${couchbaseHost}")
    private String couchbaseHost;

    @Value("${bucketName}")
    private String bucketName;

    @Value("${bucketPassword}")
    private String bucketPassword;

    public Bucket getBucket() {
        CouchbaseEnvironment env = DefaultCouchbaseEnvironment.builder()
                .connectTimeout(10000)
                .kvTimeout(3000)
                .build();

        Cluster cluster = CouchbaseCluster.create(env, couchbaseHost);
        Bucket bucket = cluster.openBucket(bucketName, bucketPassword);

        return bucket;
    }
}
