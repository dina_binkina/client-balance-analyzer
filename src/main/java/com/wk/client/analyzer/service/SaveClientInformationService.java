package com.wk.client.analyzer.service;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.json.JsonObject;
import com.couchbase.client.java.error.DocumentAlreadyExistsException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.servlet.ServletContext;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Component
public class SaveClientInformationService {

    @Value("${clientsInfoAddress}")
    String clientsIfoAddress;

    @Value("${bucketDocumentId}")
    String bucketDocumentId;

    @Value("classpath:clientInfoOutput.xsl")
    Resource resourceFile;

    @Autowired
    private BucketInitializer bucketInitializer;

    @Scheduled(fixedRate = 100000)
    public void sendHttpGetClientsRequest() throws URISyntaxException {

        RestTemplate restTemplate = new RestTemplate();
        URI uri = new URI(clientsIfoAddress);

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_XML));
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

        ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
        String responseBody = response.getBody();

        saveToCache(responseBody);
    }

    public void saveToCache(String responseBody) {
        Map map = new HashMap();
        try {
            File xslFile = resourceFile.getFile();
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer =
                    factory.newTransformer(new StreamSource(xslFile));
            StreamSource xmlSource = new StreamSource(new StringReader(responseBody));
            StringWriter writer = new StringWriter();
            StreamResult output = new StreamResult(writer);
            transformer.transform(xmlSource, output);
            String strResult = writer.toString().replaceFirst("<.*?>", "").trim();
            String[] keyValues = strResult.split("\n");

            for (String keyvalue : keyValues) {
                if (!keyvalue.isEmpty() && keyvalue.contains("/")) {
                    map.put(keyvalue.split("/", 2)[0].trim(), keyvalue.split("/", 2)[1].trim());
                }
            }

        } catch (Exception e) {
            log.error("Failed to transform xml with xslt stylesheet.", e.getMessage());
        }

        Bucket bucket = bucketInitializer.getBucket();

        JsonObject content = JsonObject.empty();
        map.forEach((key, value) -> {
            content.put(String.valueOf(key), String.valueOf(value));
        });
        JsonDocument document = JsonDocument.create(bucketDocumentId, content);

        JsonDocument inserted = null;
        try {
            inserted = bucket.upsert(document);
        } catch (DocumentAlreadyExistsException e) {
            log.warn("Document already exists {} : {}", inserted != null ? inserted.content() : inserted, e.getMessage());
        }

    }
}
