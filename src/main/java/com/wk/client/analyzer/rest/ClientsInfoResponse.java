package com.wk.client.analyzer.rest;

import com.wk.client.analyzer.pojo.ClientInfo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ClientsInfoResponse {

    private List<ClientInfo> clientsInfo;
}
