package com.wk.client.analyzer.pojo;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
public class ClientInfo {
    private String clientId;
    private String login;
    private Long tariff;
    private Long balance;
}
